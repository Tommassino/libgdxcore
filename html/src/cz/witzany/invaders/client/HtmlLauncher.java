package cz.witzany.invaders.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import cz.witzany.game.MainGame;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(MainGame.WIDTH, MainGame.HEIGHT);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new MainGame();
        }
}