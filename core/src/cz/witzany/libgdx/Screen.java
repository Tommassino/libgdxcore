/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx;

import cz.witzany.libgdx.model.IDrawable;
import cz.witzany.libgdx.model.IActive;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Screen implements IActive, IDrawable{

    //IActive
    public abstract void create();

    public abstract void update();

    public abstract void dispose();

    public abstract void pause();

    public abstract void resume();

    //IDrawable
    public abstract void render(SpriteBatch sb);

    public abstract void resize(int width, int height);
	
}