/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import cz.witzany.libgdx.entity.Entity;
import cz.witzany.libgdx.entity.TextureEntity;
import cz.witzany.libgdx.model.IPositioned;
import cz.witzany.libgdx.model.IScript;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author tommassino
 */
public class InputHandler {
    
    public static final int MAX_KEY = 255;
    private boolean[] keyStates = new boolean[MAX_KEY];
    
    public static final int MAX_TOUCH = 5;
    private int[] touchStates = new int[MAX_TOUCH];
    
    public static final int MOUSE_ID=0;
    private Vector2 mouseState = null;
    
    public Collection<Entity> attachment = null;
    
    public InputHandler(){
        for(int i=0; i<MAX_KEY; i++)
            keyStates[i]=false;
    }
    
    public void update(){
        //Keyboard
        for(int i=0; i<MAX_KEY; i++){
            boolean pressed = Gdx.input.isKeyPressed(i);
            if(pressed && !keyStates[i])
                keyPressed(i);
            if(!pressed && keyStates[i])
                keyReleased(i);
            keyStates[i]=pressed;
        }
        
        //Mouse
        boolean mouseLeft = Gdx.input.isButtonPressed(Input.Buttons.LEFT);
        Vector2 position = new Vector2(Gdx.input.getX(), Gdx.input.getY());
        position.y=480-position.y;
        if(mouseLeft && mouseState==null)
            mousePressed(MOUSE_ID, position);
        else if(!mouseLeft && mouseState!=null)
            mouseReleased(MOUSE_ID, position);
        else if(mouseLeft)
            mouseDragged(MOUSE_ID, position);
        if(mouseLeft)
            mouseState = position;
        else
            mouseState = null;
        
        //TODO Touch
        for(int i=0; i<MAX_TOUCH; i++){
        }
    }
    
    public void attach(List<Entity> collection){
        attachment = collection;
    }
    
    private void keyPressed(int key){
        System.out.println("Key down "+key);
    }
    
    private void keyReleased(int key){
        System.out.println("Key released "+key);
    }
    
    private void mousePressed(int mouse, Vector2 position){
        if(attachment==null)
            return;
        for(Entity ent : attachment)
            if(ent.getBoundingBox().contains(position))
                for(IScript script : ent.getScripts())
                    script.mousePressed(position);
    }
    
    private void mouseReleased(int mouse, Vector2 position){
        if(attachment==null)
            return;
        for(Entity ent : attachment)
            if(ent.getBoundingBox().contains(position))
                for(IScript script : ent.getScripts())
                    script.mouseReleased(position);
    }
    
    private void mouseDragged(int mouse, Vector2 position){
        if(attachment==null)
            return;
        for(Entity ent : attachment)
            if(ent.getBoundingBox().contains(position))
                for(IScript script : ent.getScripts())
                    script.mouseDragged(position);
    }
    
    
}
