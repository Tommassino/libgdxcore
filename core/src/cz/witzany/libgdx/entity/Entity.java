/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.entity;

import com.badlogic.gdx.math.Vector2;
import cz.witzany.libgdx.model.IActive;
import cz.witzany.libgdx.model.IDrawable;
import cz.witzany.libgdx.model.IScript;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author tommassino
 */
public abstract class Entity implements IActive, IDrawable, Comparable<Entity>{
    
    protected Vector2 position;
    protected boolean disposeFlag;
    protected final Collection<IScript<Entity>> scripts;
    protected final int renderPriority;
    
    public Entity(Vector2 position, int renderPriority){
        this.position = position;
        scripts = new HashSet<IScript<Entity>>();
        this.renderPriority = renderPriority;
    }

    /**
     * @return the position
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector2 position) {
        this.position = position;
    }

    @Override
    public void dispose() {
        disposeFlag = true;
    }
    
    public boolean getDisposeFlag(){
        return disposeFlag;
    }

    @Override
    public void update() {
        for(IScript<Entity> script : scripts)
            script.update();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void attachScript(IScript script) {
        scripts.add(script);
        script.attach(this);
    }

    @Override
    public int compareTo(Entity t) {
        return Integer.compare(t.renderPriority, renderPriority);
    }
    
    public Collection<IScript<Entity>> getScripts(){
        return scripts;
    }
    
    public void destroy(){
        disposeFlag = true;
    }
}
