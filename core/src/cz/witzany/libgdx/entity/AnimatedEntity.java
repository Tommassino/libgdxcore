/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import cz.witzany.libgdx.TextureManager;
import java.util.Dictionary;
import java.util.HashMap;

/**
 *
 * @author tommassino
 */
public class AnimatedEntity extends Entity{
    
    private final String animationFile;
    private Sheet animation;
    private Texture texture;
    private TextureRegion[][] regions;
    private Animation currentAnimation;
    private int currentFrame;
    private float time;
    private int spriteWidth;
    private int spriteHeight;

    public AnimatedEntity(String animationFile, Vector2 position, int renderPriority){
        super(position, renderPriority);
        this.animationFile = animationFile;
        this.currentAnimation = null;
    }
    
    @Override
    public void create() {
        //TODO make this better
        FileHandle file = Gdx.files.internal(animationFile);
        Json json = new Json();
        animation = json.fromJson(Sheet.class, file);
        
        texture = TextureManager.Instance.bind(animation.animationSheet);
        regions = new TextureRegion[animation.sheetWidth][animation.sheetHeight];
        
        spriteWidth = texture.getWidth()/animation.sheetWidth;
        spriteHeight = texture.getHeight()/animation.sheetHeight;
        for(int x=0; x<animation.sheetWidth; x++)
            for(int y=0; y<animation.sheetHeight; y++){
                regions[x][y]=new TextureRegion(texture, x*spriteWidth, y*spriteHeight, spriteWidth, spriteHeight);
            }
    }
    
    public void playAnimation(String animationKey){
        if(!animation.animations.containsKey(animationKey))
            return; //possibly throw something
        
        currentAnimation = animation.animations.get(animationKey);
        currentFrame = 0;
        time = 0;
    }

    @Override
    public void render(SpriteBatch sb) {
        time+=Gdx.graphics.getDeltaTime();
        if(currentAnimation==null)
            return;
        
        if(time>=currentAnimation.frames[currentFrame].duration){
            currentFrame++;
            if(currentAnimation.frames.length<=currentFrame)
                if(currentAnimation.repeat)
                    currentFrame=0;
                else{
                    currentAnimation=null;
                    return;
                }
            time = 0;
        }
        
        Frame fr = currentAnimation.frames[currentFrame];
        sb.draw(regions[fr.x][fr.y],position.x, position.y);
    }

    @Override
    public void resize(int width, int height) {
        //TODO
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector2 getSize() {
        //TODO
        return new Vector2(spriteWidth,spriteHeight);
    }

    @Override
    public Rectangle getBoundingBox() {
        //TODO
        return new Rectangle(position.x, position.y, spriteWidth, spriteHeight);
    }
    
    public static class Sheet{
        public String animationSheet;
        public int sheetWidth;
        public int sheetHeight;
        public HashMap<String,Animation> animations;
    }
    
    public static class Animation{
        public boolean repeat;
        public Frame[] frames;
    }
    
    public static class Frame{
        public int x;
        public int y;
        public float duration;
    }
    
}
