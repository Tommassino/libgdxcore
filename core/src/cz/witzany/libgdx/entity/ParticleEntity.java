/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author tommassino
 */
public class ParticleEntity extends Entity{

    private final String particleName;
    private ParticleEffect particleEffect;

    public ParticleEntity(String particle, Vector2 position, int renderPriority) {
        super(position, renderPriority);
        this.particleName=particle;
    }
    
    @Override
    public void create() {
        particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal(particleName), Gdx.files.internal(""));
        particleEffect.getEmitters().first().setPosition(position.x, position.y);
        particleEffect.start();
    }

    @Override
    public void render(SpriteBatch sb) {
        particleEffect.update(Gdx.graphics.getDeltaTime());
        particleEffect.draw(sb);
        if(particleEffect.isComplete())
            particleEffect.reset();
    }

    @Override
    public void resize(int width, int height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector2 getSize() {
        return Vector2.Zero;
    }

    @Override
    public Rectangle getBoundingBox() {
        return new Rectangle(position.x,position.y,0,0);
    }
    
}
