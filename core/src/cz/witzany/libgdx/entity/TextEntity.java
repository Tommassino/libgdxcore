/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.entity;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author tommassino
 */
public class TextEntity extends Entity{
    
    private BitmapFont font;
    private String text;

    public TextEntity(Vector2 position, int renderPriority) {
        super(position, renderPriority);
    }

    @Override
    public void create() {
        font = new BitmapFont();
    }

    @Override
    public void render(SpriteBatch sb) {
        font.draw(sb, text, getPosition().x, getPosition().y);
    }

    @Override
    public void resize(int width, int height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    public Rectangle getBoundingBox() {
        return new Rectangle(getPosition().x, getPosition().y, getSize().x, getSize().y);
    }

    @Override
    public Vector2 getSize() {
        return new Vector2(0, 0);
    }
    
}
