/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import cz.witzany.libgdx.model.ICollidable;
import cz.witzany.libgdx.TextureManager;

/**
 *
 * @author tommassino
 */
public class TextureEntity extends Entity implements ICollidable{
    
    protected Texture texture;
    private final String texturePath;
    
    protected int width,height;
    protected boolean flipX, flipY;
    private Vector2 origin;
    
    protected int collisionMask = 0x0;
    
    public TextureEntity(String texturePath, Vector2 position, int renderPriority){
        super(position, renderPriority);
        origin = new Vector2();
        flipX = false;
        flipY = false;
        this.texturePath = texturePath;
    }
    
    @Override
    public void render(SpriteBatch batch){
        batch.draw(texture, getPosition().x-getOrigin().x, getPosition().y-getOrigin().y, 0, 0, width, height, width/texture.getWidth(), height/texture.getHeight(), 0, 0, 0, width, height, flipX, flipY);
    }
    
    public void setFlipX(boolean flip){
        flipX = flip;
    }
    
    public void setFlipY(boolean flip){
        flipY = flip;
    }
    
    @Override
    public void setCollisionMask(int collisionMask){
        this.collisionMask = collisionMask;
    }
    
    @Override
    public int getCollisionMask(){
        return collisionMask;
    }
    
    @Override
    public boolean collidesWith(ICollidable entity){
        return (collisionMask&entity.getCollisionMask()) > 0 && getBoundingBox().overlaps(entity.getBoundingBox());
    }
    
    @Override
    public Rectangle getBoundingBox(){
        return new Rectangle(getPosition().x-getOrigin().x, getPosition().y-getOrigin().y, width, height);
    }

    @Override
    public void create() {
        this.texture = TextureManager.Instance.bind(texturePath);
        this.width = texture.getWidth();
        this.height = texture.getHeight();
    }

    @Override
    public void dispose() {
        super.dispose();
        texture.dispose();
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * @return the origin
     */
    public Vector2 getOrigin() {
        return origin;
    }

    /**
     * @param origin the origin to set
     */
    public void setOrigin(Vector2 origin) {
        this.origin = origin;
    }
    
    public Vector2 getOriginPosition(){
        return getPosition().cpy().add(origin);
    }

    @Override
    public String toString() {
        return "TextureEntity: "+texturePath+"\t"+getPosition().toString()+"\tw:"+texture.getWidth()+"\th:"+texture.getHeight();
    }

    @Override
    public Vector2 getSize() {
        return new Vector2(width,height);
    }
}
