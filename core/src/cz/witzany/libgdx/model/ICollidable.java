/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.model;

import com.badlogic.gdx.math.Rectangle;

/**
 *
 * @author tommassino
 */
public interface ICollidable extends IPositioned{
    
    public void setCollisionMask(int collisionMask);
    public int getCollisionMask();
    public boolean collidesWith(ICollidable collidable);
}
