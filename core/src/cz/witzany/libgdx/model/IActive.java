/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.model;

/**
 *
 * @author tommassino
 */
public interface IActive {
    public void attachScript(IScript script);
    public void create();
    public void dispose();
    public void update();
    public void pause();
    public void resume();
}
