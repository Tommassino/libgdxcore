/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.model;

import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author tommassino
 */
public abstract class IScript<TEntity extends IActive> {
    
    public abstract void attach(TEntity entity);
    
    public abstract void update();
    
    public void handleCollision(TEntity entity){
    }
    
    public void mousePressed(Vector2 position){}
    
    public void mouseReleased(Vector2 position){}
    
    public void mouseDragged(Vector2 position){}
}
