/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author tommassino
 */
public interface IDrawable extends IPositioned {
    public abstract void render(SpriteBatch sb);

    public abstract void resize(int width, int height);
}
