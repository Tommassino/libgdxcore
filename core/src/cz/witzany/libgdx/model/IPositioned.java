/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author tommassino
 */
public interface IPositioned {
    public Vector2 getPosition();
    public Vector2 getSize();
    public Rectangle getBoundingBox();
}
