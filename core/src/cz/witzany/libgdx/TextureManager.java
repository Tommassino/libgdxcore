/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import java.util.HashMap;

/**
 *
 * @author tommassino
 */
public class TextureManager {
    
    public static final TextureManager Instance = new TextureManager();
    
    private HashMap<String,TextureLease> textureCache;
    
    private TextureManager(){
        textureCache = new HashMap<String, TextureLease>();
    }
    
    public Texture bind(String texture){
        TextureLease lease = null;
        if(textureCache.containsKey(texture))
            lease = textureCache.get(texture);
        else{
            lease = new TextureLease(texture);
            textureCache.put(texture, lease);
        }
        lease.bind();
        return lease.texture();
    }
    
    public void unbind(String texture){
        if(textureCache.containsKey(texture)){
            TextureLease lease = textureCache.get(texture);
            lease.unbind();
        }
    }
    
    private class TextureLease{
        private String texture;
        private Texture loaded;
        private int binds;
        
        public TextureLease(String texture){
            this.texture = texture;
        }
        
        public void bind(){
            binds++;
        }
        
        public void unbind(){
            binds--;
            if(binds<=0 && loaded!=null)
                unloadTexture();
        }
        
        public Texture texture(){
            if(binds<=0)
                throw new IllegalStateException("Texture is not bound.");
            if(loaded==null)
                loadTexture();
            return loaded;
        }
        
        public void loadTexture(){
            loaded = new Texture(Gdx.files.internal(texture));
        }
        
        public void unloadTexture(){
            if(loaded!=null)
                loaded.dispose();
        }
    }
}
