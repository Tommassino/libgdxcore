/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import java.util.HashMap;

/**
 *
 * @author tommassino
 */
public class SoundManager {
    public static SoundManager Instance = new SoundManager();
    
    private HashMap<String,Sound> soundCache;
    
    private SoundManager(){
        soundCache = new HashMap<String, Sound>();
    }
    
    public void playSound(String sound){
        if(!soundCache.containsKey(sound))
            soundCache.put(sound, Gdx.audio.newSound(Gdx.files.internal(sound)));
        soundCache.get(sound).play();
    }
}
