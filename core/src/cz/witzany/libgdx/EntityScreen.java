/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.libgdx;

import cz.witzany.libgdx.model.ICollidable;
import cz.witzany.libgdx.model.IScript;
import cz.witzany.libgdx.entity.Entity;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 *
 * @author tommassino
 */
public class EntityScreen extends Screen{
    
    //TODO make entitySet a list ordered by priority
    private List<Entity> entitySet;
    private Collection<Entity> newEntities;
    private Collection<IScript<EntityScreen>> scripts;
    private int width, height;
    private OrthoCamera camera;
    private InputHandler handler;
    private boolean paused;
    private boolean created;
    
    public EntityScreen(int width, int height){
        this.width = width;
        this.height = height;
        newEntities = new HashSet<Entity>();
        entitySet = new ArrayList<Entity>();
        scripts = new HashSet<IScript<EntityScreen>>();
        paused = false;
        created = false;
        handler = new InputHandler();
        handler.attach(entitySet);
    }
    
    public OrthoCamera getCamera(){
        return camera;
    }

    @Override
    public void create() {
        camera = new OrthoCamera(width, height);
        for(Entity entity : entitySet)
            entity.create();
        created = true;
    }

    @Override
    public void update() {
        for(Entity toAdd : newEntities){
            toAdd.create();
            entitySet.add(toAdd);
        }
        newEntities.clear();
        
        handler.update();
        
        camera.update();
        if(isPaused())
            return;
        
        Collections.sort(entitySet);
        //TODO self sorting list
        
        List<Entity> toDispose = new ArrayList<Entity>();
        for(Entity entity : entitySet){
            entity.update();
            if(entity.getDisposeFlag())
                toDispose.add(entity);
        }
        
        for(int i = 0; i<entitySet.size(); i++){
            Entity ient = entitySet.get(i);
            if(ient instanceof ICollidable){
                ICollidable icol = (ICollidable)ient;
                for(int j=i+1; j<entitySet.size(); j++){
                    Entity jent = entitySet.get(j);
                    if(jent instanceof ICollidable){
                        ICollidable jcol = (ICollidable)jent;
                        if(icol.collidesWith(jcol)){
                            for(IScript script : ient.getScripts())
                                script.handleCollision(jent);
                        }
                        if(jent.getDisposeFlag())
                            toDispose.add(jent);
                    }
                }
                if(ient.getDisposeFlag())
                    toDispose.add(ient);
            }
        }
        
        for(Entity dispose : toDispose)
            entitySet.remove(dispose);
        
        for(IScript script : scripts)
            script.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        for(Entity entity : entitySet)
            entity.render(sb);
        sb.end();
    }

    @Override
    public void resize(int width, int height) {
        if(camera != null)
            camera.resize(width, height);
        this.width = width;
        this.height = height;
    }

    @Override
    public void dispose() {
        for(Entity entity : entitySet)
            entity.dispose();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void attachScript(IScript script) {
        script.attach(this);
        scripts.add(script);
    }
    
    public void add(Entity entity){
        if(created)
            newEntities.add(entity);
        else
            entitySet.add(entity);
    }

    /**
     * @return the paused
     */
    public boolean isPaused() {
        return paused;
    }

    @Override
    public Vector2 getPosition() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector2 getSize() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Rectangle getBoundingBox() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
