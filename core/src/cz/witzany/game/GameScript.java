/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.witzany.game;

import com.badlogic.gdx.math.Vector2;
import cz.witzany.libgdx.EntityScreen;
import cz.witzany.libgdx.ScreenManager;
import cz.witzany.libgdx.entity.AnimatedEntity;
import cz.witzany.libgdx.entity.ParticleEntity;
import cz.witzany.libgdx.entity.TextureEntity;
import cz.witzany.libgdx.model.IScript;

/**
 *
 * @author tommassino
 */
public class GameScript extends IScript<EntityScreen>{

    private EntityScreen screen;
    private AnimatedEntity animation;
    private boolean playAnimation = false;
    
    @Override
    public void attach(EntityScreen entity) {
        screen = entity;
        
        animation = new AnimatedEntity("test.animation", new Vector2(100,100), 0);
        screen.add(animation);
        
        screen.add(new TextureEntity("test.png",new Vector2(200, 0),0));
        
        screen.add(new ParticleEntity("test.particle", new Vector2(300,300), 0));
    }

    @Override
    public void update() {
        //animation.update();
        if(!playAnimation){
            animation.playAnimation("test");
            playAnimation=true;
        }
    }
    
}
