package cz.witzany.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import cz.witzany.libgdx.EntityScreen;
import cz.witzany.libgdx.ScreenManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainGame extends ApplicationAdapter  {
	
    public static final int WIDTH = 800, HEIGHT = 480;
    private SpriteBatch batch;

    @Override
    public void create () {
        batch = new SpriteBatch();
        EntityScreen mainScreen = new EntityScreen(WIDTH, HEIGHT);
        mainScreen.attachScript(new GameScript()); 
        ScreenManager.setScreen(mainScreen);
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        if(ScreenManager.getCurrentScreen()!=null)
            ScreenManager.getCurrentScreen().update();
        
        if(ScreenManager.getCurrentScreen()!=null)
            ScreenManager.getCurrentScreen().render(batch);

    }

    @Override
    public void dispose() {
        super.dispose(); //To change body of generated methods, choose Tools | Templates.
        if(ScreenManager.getCurrentScreen()!=null)
            ScreenManager.getCurrentScreen().dispose();
    }

    @Override
    public void pause() {
        super.pause(); //To change body of generated methods, choose Tools | Templates.
        if(ScreenManager.getCurrentScreen()!=null)
            ScreenManager.getCurrentScreen().pause();
    }

    @Override
    public void resume() {
        super.resume(); //To change body of generated methods, choose Tools | Templates.
        if(ScreenManager.getCurrentScreen()!=null)
            ScreenManager.getCurrentScreen().resume();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height); //To change body of generated methods, choose Tools | Templates.
        if(ScreenManager.getCurrentScreen()!=null)
            ScreenManager.getCurrentScreen().resize(width, height);
    }
}
